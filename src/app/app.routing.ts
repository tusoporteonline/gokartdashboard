import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './views/paginas/login/login.component'
import { VerificationService } from './services/verification.service'
// Import Containers
import {
  FullLayout,
  SimpleLayout
} from './containers';

export const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
    canActivate: [VerificationService]
  },
  {
    path: '',
    component: FullLayout,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule',
        canActivate: [VerificationService]
      },
      {
        path: 'cars',
        loadChildren: './views/cars/cars.module#CarsModule',
        canActivate: [VerificationService]
      },
      {
        path: 'clientes',
        loadChildren: './views/clients/clients.module#ClientsModule',
        canActivate: [VerificationService]
      },
      {
        path: 'carreras',
        loadChildren: './views/career/career.module#CareerModule',
        canActivate: [VerificationService]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
