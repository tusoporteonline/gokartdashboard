import { Injectable } from '@angular/core';

import * as _swal from 'sweetalert';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = _swal as any;

@Injectable()
export class MensajesService {

  constructor() { }
  successMsg(mensaje, titulo) {
    swal(titulo, mensaje, "success");
  }
  errorMsg(mensaje, titulo) {
    swal(titulo, mensaje, "error");
  }
  warningMsg(mensaje, titulo) {
    // sweetalert("Hello world!");
    // swal('test');
    swal(titulo, mensaje, "warning");
  }
}
