import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class HttpWebServicesService {

  public url: string;
  public data: object;


  constructor(private http: Http) {
  }

  getFunction() {
    return this.http.get(this.url).map(res => res.json());
  }

  postFunction() {
    return this.http.post(this.url, this.data).map(res => res.json());
  }

  putFunction() {
    return this.http.put(this.url, this.data).map(res => res.json());
  }

  deleteFunction() {
    return this.http.delete(this.url).map(res => res.json());
  }

}
