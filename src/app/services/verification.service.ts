import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';

@Injectable()
export class VerificationService implements CanActivate {


  constructor(private router: Router) { }

  canActivate() {
    var ok = true;
    console.log(localStorage.getItem("token"));
    if (localStorage.getItem("token") != null) {
      console.log("Encontro un token");
      ok = true;
    } else {
      this.router.navigate(['/login']);
      console.log("No encontro ningun token");
    }
    return ok;
  }



}
