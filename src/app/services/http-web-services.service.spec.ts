import { TestBed, inject } from '@angular/core/testing';

import { HttpWebServicesService } from './http-web-services.service';

describe('HttpWebServicesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpWebServicesService]
    });
  });

  it('should be created', inject([HttpWebServicesService], (service: HttpWebServicesService) => {
    expect(service).toBeTruthy();
  }));
});
