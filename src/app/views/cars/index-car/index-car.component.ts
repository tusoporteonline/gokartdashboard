import { Component, OnInit } from '@angular/core';

import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { Cars } from '../../../class/cars';
import { HttpWebServicesService } from '../../../services/http-web-services.service';
import { environment } from '../../../../environments/environment';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-index-car',
  templateUrl: './index-car.component.html',
  styleUrls: ['./index-car.component.scss'],
  providers: [HttpWebServicesService]
})
export class IndexCarComponent implements OnInit {

  public cars = new Cars();
  private url = null;
  private idCar = 0;
  constructor(private http: HttpWebServicesService, private activatedRoute: ActivatedRoute) {
  }

  private idCas = 0;
  ngOnInit() {
    this.obtenerConfiguracion();
    this.activatedRoute.params.subscribe(data => this.idCar = parseInt(data['idCars']));
    if (this.idCar > 0) {
      this.http.url = `${this.url}car/getCar/${this.idCar}`;
      this.http.getFunction().subscribe(res => {
        this.cars = res.data[0];
      });
    }

  }

  public obtenerConfiguracion() {
    if (environment.production == false) {
      this.url = environment.configDesarrollo.url;
    } else {
      this.url = environment.configProduccion.url;
    }
  }


  guardarVehiculo() {
    this.http.data = this.cars;

    if (this.cars.idvehiculo > 0) {
      this.http.url = `${this.url}car/putCar`;
      this.http.putFunction().subscribe(res => {
        if (res.status == 200) {
          console.log(res);
        }
      });
    } else {
      this.http.url = `${this.url}car/saveCar`;
      this.http.postFunction().subscribe(res => {
        console.log(res);
      });
    }
  }

}
