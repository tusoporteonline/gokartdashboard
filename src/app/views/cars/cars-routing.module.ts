import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexCarComponent } from './index-car/index-car.component';
import { ListCarsComponent } from './list-cars/list-cars.component'



const routes: Routes = [
  {
    path: 'altas',
    component: IndexCarComponent,
    data: {
      title: 'Altas'
    }
  },
  {
    path: 'lista',
    component: ListCarsComponent,
    data: {
      title: "Lista"
    }
  },
  {
    path: 'editar/:idCars',
    component: IndexCarComponent,
    data: {
      title: 'Editar'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarsRoutingModule { }
