import { Component, OnInit } from '@angular/core';
import { HttpWebServicesService } from '../../../services/http-web-services.service'
import { environment } from '../../../../environments/environment'

@Component({
  selector: 'app-list-cars',
  templateUrl: './list-cars.component.html',
  styleUrls: ['./list-cars.component.scss'],
  providers: [HttpWebServicesService]
})
export class ListCarsComponent implements OnInit {
  private url = null;
  lstCarros = [];
  constructor(private http: HttpWebServicesService) {

  }

  ngOnInit() {
    this.obtenerConfiguracion();
    this.loadCars();
  }


  public obtenerConfiguracion() {
    if (environment.production == false) {
      this.url = environment.configDesarrollo.url;
    } else {
      this.url = environment.configProduccion.url;
    }
  }


  private loadCars() {
    this.http.url = `${this.url}car/getCars`;
    this.http.getFunction().subscribe(res => {
      this.lstCarros = res.data;
      console.log(res);
    });
  }


}
