import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComboClientesComponent } from './combo-clientes.component';

describe('ComboClientesComponent', () => {
  let component: ComboClientesComponent;
  let fixture: ComponentFixture<ComboClientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComboClientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComboClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
