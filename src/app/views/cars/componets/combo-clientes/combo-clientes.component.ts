import { Component, OnInit } from '@angular/core';
import { HttpWebServicesService } from '../../../../services/http-web-services.service';

@Component({
  selector: 'app-combo-clientes',
  templateUrl: './combo-clientes.component.html',
  styleUrls: ['./combo-clientes.component.scss']
})
export class ComboClientesComponent implements OnInit {

  constructor(private servHttp: HttpWebServicesService) { }

  ngOnInit() {
  }

}
