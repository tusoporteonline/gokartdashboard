import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarsRoutingModule } from './cars-routing.module';
import { ListCarsComponent } from './list-cars/list-cars.component';
import { IndexCarComponent } from './index-car/index-car.component';
import { ComboClientesComponent } from './componets/combo-clientes/combo-clientes.component';
import { FormsModule } from '@angular/forms';
import { VerificationService } from '../../services/verification.service'



@NgModule({
  imports: [
    CommonModule,
    CarsRoutingModule,
    FormsModule,
  ],
  declarations: [ListCarsComponent, IndexCarComponent, ComboClientesComponent]
})
export class CarsModule {

  constructor(){

  }


}
