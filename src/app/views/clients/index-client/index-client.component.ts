import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpWebServicesService } from '../../../services/http-web-services.service'
import { environment } from '../../../../environments/environment'

@Component({
  selector: 'app-index-client',
  templateUrl: './index-client.component.html',
  styleUrls: ['./index-client.component.scss'],
  providers: [HttpWebServicesService]
})
export class IndexClientComponent implements OnInit {
  private url = null;
  private objCliente: object;
  private idCliente = 0;


  constructor(private activatedRoute: ActivatedRoute, private http: HttpWebServicesService) { }

  ngOnInit() {
    this.obtenerConfiguracion();
    this.activatedRoute.params.subscribe(data => this.idCliente = parseInt(data['idCliente']));

    this.dameInformacionCliente();
  }



  public obtenerConfiguracion() {
    if (environment.production == false) {
      this.url = environment.configDesarrollo.url;
    } else {
      this.url = environment.configProduccion.url;
    }
  }

  private dameInformacionCliente() {
    this.http.url = `${this.url}client/getClient/${this.idCliente}`;
    this.http.getFunction().subscribe(res => {
      this.objCliente = res.data[0];
      console.log(this.objCliente);

    })
  }




}
