import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ClientsRoutingModule } from './clients-routing.module';
import { IndexClientComponent } from './index-client/index-client.component';
import { ListClientsComponent } from './list-clients/list-clients.component';

@NgModule({
  imports: [
    CommonModule,
    ClientsRoutingModule,
    FormsModule
  ],
  declarations: [IndexClientComponent, ListClientsComponent]
})
export class ClientsModule { }
