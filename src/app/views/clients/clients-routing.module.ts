import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexClientComponent } from './index-client/index-client.component';
import { ListClientsComponent } from './list-clients/list-clients.component';


const routes: Routes = [
  {
    path: 'altas',
    component: IndexClientComponent,
    data: {
      title: 'Altas'
    }
  },
  {
    path: 'lista',
    component: ListClientsComponent,
    data: {
      title: "Lista"
    }
  },
  {
    path: 'editar/:idCliente',
    component: IndexClientComponent,
    data: {
      title: 'Editar'
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
