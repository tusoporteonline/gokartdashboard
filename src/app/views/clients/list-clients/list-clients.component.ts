import { Component, OnInit } from '@angular/core';
import { HttpWebServicesService } from '../../../services/http-web-services.service'
import { environment } from '../../../../environments/environment'

@Component({
  selector: 'app-list-clients',
  templateUrl: './list-clients.component.html',
  styleUrls: ['./list-clients.component.scss'],
  providers: [HttpWebServicesService]
})
export class ListClientsComponent implements OnInit {
  private url = null;
  lstClientes = [];
  constructor(private http: HttpWebServicesService) { }

  ngOnInit() {
    this.obtenerConfiguracion();
    this.loadDataClient();
  }



  public obtenerConfiguracion() {
    if (environment.production == false) {
      this.url = environment.configDesarrollo.url;
    } else {
      this.url = environment.configProduccion.url;
    }
  }

  public loadDataClient() {
    this.http.url = `${this.url}client/getClients/`
    this.http.getFunction().subscribe(res => {
      this.lstClientes = res.data;
    });
  }
}
