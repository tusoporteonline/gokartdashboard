import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpWebServicesService } from '../../../services/http-web-services.service';
import { Socket } from 'ng-socket-io';




@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers: [HttpWebServicesService]
})


export class ListComponent implements OnInit {
  public lstHeadCareer = [];
  private url = null;
  constructor(private http: HttpWebServicesService, private sock: Socket) { }

  ngOnInit() {
    this.obtenerConfiguracion();
    this.getInfoHeadCareer();


  }

  public obtenerConfiguracion() {
    if (environment.production == false) {
      this.url = environment.configDesarrollo.url;
    } else {
      this.url = environment.configProduccion.url;
    }
  }

  private getInfoHeadCareer() {
    this.http.url = `${this.url}careers/infoHeadCareers/`
    this.http.getFunction().subscribe(res => {
      console.log(res);
      this.lstHeadCareer = res.data;
    })
  }


  public activatecareer(idHeadCareer) {
    console.log("Va activar la carrera");
    this.sock.emit("activateCareer", idHeadCareer);
  }

}
