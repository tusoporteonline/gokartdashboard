import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CareerRoutingModule } from './career-routing.module';
import { ListComponent } from './list/list.component';
import { IndexComponent } from './index/index.component';
import { FormsModule } from '@angular/forms';
import { DriversComponent } from './drivers/drivers.component';

@NgModule({
  imports: [
    CommonModule,
    CareerRoutingModule,
    FormsModule,
  ],
  declarations: [ListComponent, IndexComponent, DriversComponent]
})
export class CareerModule { }
