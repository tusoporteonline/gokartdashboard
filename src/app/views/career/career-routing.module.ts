import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { IndexComponent } from './index/index.component';
import { ListComponent } from './list/list.component';
import { DriversComponent } from './drivers/drivers.component';

const routes: Routes = [
  {
    path: 'altas',
    component: IndexComponent,
    data: {
      title: 'Altas'
    }
  },
  {
    path: 'lista',
    component: ListComponent,
    data: {
      title: 'Altas'
    }
  },
  {
    path: 'editar/:idCareer',
    component: IndexComponent,
    data: {
      title: 'Edicion'
    }
  },
  {
    path: 'driver/:idCareer',
    component: DriversComponent,
    data: {
      title: 'Asignacion de coductores'
    }
  }





];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CareerRoutingModule { }
