import { Component, OnInit } from '@angular/core';
import { HttpWebServicesService } from '../../../services/http-web-services.service'
import { environment } from '../../../../environments/environment'
import { ActivatedRoute } from '@angular/router';
import { HeadCareer } from '../../../class/headCareer';
import { CareerUser } from '../../../class/careerUser';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.scss'],
  providers: [HttpWebServicesService]
})
export class DriversComponent implements OnInit {
  private lstClientes = [];
  // private idCareer = 0;
  public headCareer = new HeadCareer();
  public objCareeruser = new CareerUser();
  public lstCarros = [];
  public lstUserCareer = [];
  private url = null;

  constructor(private http: HttpWebServicesService, private activatedRoute: ActivatedRoute) {
    this.obtenerConfiguracion();
    this.loadInfoHeadCareer();
  }

  ngOnInit() {

    this.loadDataClientAvaliable();
    this.loadCars();
    this.loadTableUserCareer();

  }

  public loadInfoHeadCareer() {
    this.activatedRoute.params.subscribe(data => this.objCareeruser.idCarreraEncabezado = parseInt(data['idCareer']));
    if (this.objCareeruser.idCarreraEncabezado > 0) {
      this.http.url = `${this.url}careers/infoHeadCareer/${this.objCareeruser.idCarreraEncabezado}`;
      this.http.getFunction().subscribe(res => {
        this.headCareer = res.data[0];
      });
    }
  }



  public obtenerConfiguracion() {
    if (environment.production == false) {
      console.log("Va a entrar a desarrollo");
      this.url = environment.configDesarrollo.url;
    } else {
      this.url = environment.configProduccion.url;
    }
  }


  public loadDataClientAvaliable() {
    this.http.url = `${this.url}client/getClients/`
    this.http.getFunction().subscribe(res => {
      console.log("Esta es la informacion de los clientes disponibles");
      console.log(res);

      this.lstClientes = res.data;
    });
  }


  private loadCars() {
    this.http.url = `${this.url}car/getCars`;
    this.http.getFunction().subscribe(res => {
      console.log("Informacion de los clientes disponibles");
      console.log(res);
      this.lstCarros = res.data;
      console.log(this.lstCarros);
    });
  }

  private loadTableUserCareer() {
    this.http.url = `${this.url}careers/userCareer/${this.objCareeruser.idCarreraEncabezado}`;
    this.http.getFunction().subscribe(res => {
      console.log("******************");
      console.log(res);
      console.log("******************");
      if (res.status == 200) {
        this.lstUserCareer = res.data;
        console.log(this.lstUserCareer);
      }
    });
  }

  public guardarCareerUser() {
    this.http.data = this.objCareeruser;
    this.http.url = `${this.url}careers/saveCareerUser`;
    this.http.postFunction().subscribe(res => {
      console.log("------------- ----");
      console.log(res);
      this.loadDataClientAvaliable();
      // if (res.status == 200) {
      this.loadTableUserCareer();
      // }
    });
  }


  public eliminarIntegrante(idUserCareer) {
    console.log("Va a entrar a eliminar");
    console.log(idUserCareer);
    this.http.url = `${this.url}careers/deleteDrivers/${idUserCareer}`;
    this.http.deleteFunction().subscribe(res => {
      console.log(res);
      if (res.status == 201) {
        console.log("Nuevo registro disponible");
        this.loadTableUserCareer();
      }
    });
  }
}
