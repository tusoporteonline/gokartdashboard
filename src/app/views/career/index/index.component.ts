import { Component, OnInit } from '@angular/core';
import { HeadCareer } from '../../../class/headCareer';
import { environment } from '../../../../environments/environment';
import { HttpWebServicesService } from '../../../services/http-web-services.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [HttpWebServicesService]
})
export class IndexComponent implements OnInit {

  public headCareer = new HeadCareer();
  private url =null;
  private idCareer = 0;
  constructor(private http: HttpWebServicesService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(data => this.idCareer = parseInt(data['idCareer']));
    if (this.idCareer > 0) {
      this.http.url = `${this.url}careers/infoHeadCareer/${this.idCareer}`;
      this.http.getFunction().subscribe(res => {
        this.headCareer = res.data[0];
        console.log(res);
      });
      console.log(this.idCareer);
    }
    this.obtenerConfiguracion();

  }

  public obtenerConfiguracion() {
    if (environment.production == false) {
      this.url = environment.configDesarrollo.url;
    } else {
      this.url = environment.configProduccion.url;
    }
  }


  guardarHeadCareer() {
    this.http.data = this.headCareer;
    if (this.idCareer > 0) {
      this.http.url = `${this.url}careers/updateHeadCareer`;
      this.http.putFunction().subscribe(res => {
        if (res.status == 201) {
          alert("Registro actualizado");
        }
      });
    } else {
      this.http.url = `${this.url}careers/saveHeadCareer/`;
      this.http.postFunction().subscribe(res => {
        if (res.status == 201) {
          alert("Nuevo registro disponible");
          this.limpiar();
        }
      })
    }
  }


  private limpiar() {
    this.headCareer = new HeadCareer();
  }



}
