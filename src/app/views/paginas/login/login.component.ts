import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VerificationService } from '../../../services/verification.service';
import { HttpWebServicesService } from '../../../services/http-web-services.service';
import { environment } from '../../../../environments/environment';
import { MensajesService } from '../../../services/Utilerias/mensajes.service';
import { tick } from '@angular/core/testing';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [VerificationService, HttpWebServicesService]

})
export class LoginComponent implements OnInit {
  private objUser = {};
  private url = null;

  constructor(private router: Router,
    private auth: VerificationService,
    private http: HttpWebServicesService,
    private msg: MensajesService) {

  }
  ngOnInit() {
    this.obtenerConfiguracion();
    //console.log(this.auth.verificationToken());
  }



  public obtenerConfiguracion() {
    if (environment.production == false) {
      this.url = environment.configDesarrollo.url;
    } else {
      this.url = environment.configProduccion.url;
    }
  }
  public login() {
    this.http.url = `${this.url}login/login`;
    this.http.data = this.objUser;
    this.http.postFunction().subscribe(res => {
      console.log(res);
      if (res.status == 200) {
        console.log(res.data);
        localStorage.setItem("token", res.data[0].idusaurioSistema);
        this.router.navigate(['/dashboard']);
      }else{
        this.msg.warningMsg("Acceso denegado", "Ups");
      }
    });

  }


}
